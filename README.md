# JavaSubmissionTester
This gem add java support for "Hier könnte ihr Name stehen". It only runs on linux at the moment because it uses the program ```timeout``` to stop tests after 2 minutes.

## Installation
Install java on your System.
Add this line to your application's Gemfile:

```ruby
gem 'java_submission_tester', git: 'https://git.thm.de/dalt40/java-submission-tester.git', branch: 'master'
```

And then execute:
```bash
$ bundle
```

Configure Java Paths:

```ruby
    # config/application.rb or config/environments/*.rb
    # java paths
    JavaSubmissionTester.java_8_path = {
        java: '/usr/lib/jvm/java-8-openjdk/bin/java',
        javac: '/usr/lib/jvm/java-8-openjdk/bin/javac',
        javap: '/usr/lib/jvm/java-8-openjdk/bin/javap'
    }
    JavaSubmissionTester.java_9_path = {
        java: '/usr/lib/jvm/java-9-jdk/bin/java',
        javac: '/usr/lib/jvm/java-9-jdk/bin/javac',
        javap: '/usr/lib/jvm/java-9-jdk/bin/javap'
    }
```

## Useful Information
1. The java source code for JunitReport.jar can be found in the java folder.
2. JunitJSONReportRunner is for running the test and generation a report in the json format. It uses Junits RunListener.
3. JunitJSONTestList is for listing the test cases defined in any given java class. It just looks for all methods annotated with @Test and used the method name as testcase name.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
