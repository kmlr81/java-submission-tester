package de.thm.mni;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by dominic-althaus on 19.05.16.
 */
public class JunitJSONTestList {
    public static void main(String[] args) {
        JSONObject testMethods = new JSONObject();

        if(args.length <= 0) System.exit(1);
        try {
            Class testClass = Class.forName(args[0]);

            for(Method m : testClass.getMethods()) {
                if(m.isAnnotationPresent(org.junit.Test.class)) {
                    JSONObject testStructure = new JSONObject();
                    JSONObject extras = new JSONObject();
                    testMethods.put(m.getName(), testStructure);

                    testStructure.put("extras", extras);

                    for(Annotation annotation : m.getAnnotations()) {
                        switch(annotation.annotationType().getName()) {
                            case "org.junit.Test":
                                //Ignore
                                break;
                            case "de.thm.mni.Points":
                                Points points = (Points)annotation;
                                testStructure.put("points", points.value());
                                break;
                            default:
                                JSONObject extra = new JSONObject();
                                extras.put(annotation.annotationType().getName(), extra);

                                for(Method method : annotation.annotationType().getMethods()) {
                                    if(method.getDeclaringClass() == Annotation.class) {
                                        continue;
                                    }

                                    String name = method.getName();
                                    Object value = method.invoke(annotation);

                                    extra.put(name, value);
                                }
                        }
                    }
                }
            }

            System.out.println(testMethods);
        } catch (ClassNotFoundException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
            System.exit(2);
        }
    }
}
