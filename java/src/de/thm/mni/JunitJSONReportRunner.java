package de.thm.mni;

import org.json.JSONObject;
import org.junit.runner.Description;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

import java.io.IOException;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JunitJSONReportRunner {

    public static void main(String[] args) {
        if(args.length <= 1) System.exit(1);
        try {
            Class testClass = Class.forName(args[1]);
            final JSONRunListener runListener = new JSONRunListener(args[0]);
            JUnitCore jUnitCore = new JUnitCore();

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    runListener.writeTestResult();
                }
            });

            jUnitCore.addListener(runListener);
            jUnitCore.run(testClass);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(2);
        }
    }

    static class JSONRunListener extends RunListener {
        JSONObject jsonResult;
        Path outputPath;

        JSONRunListener(String outputPath) {
            this.outputPath = Paths.get(outputPath);
            jsonResult = new JSONObject();
        }

        @Override
        public void testRunStarted(Description description) throws Exception {
        }

        @Override
        public void testStarted(Description description) throws Exception {
            JSONObject testResult = new JSONObject();
            String testName = description.getMethodName();
            jsonResult.put(testName, testResult);
        }

        @Override
        public void testFailure(Failure failure) throws Exception {
            JSONObject testResult = jsonResult.getJSONObject(failure.getDescription().getMethodName());

            testResult.put("successful", false);
            testResult.put("failureMessage", failure.getException().toString());
            testResult.put("failureException", failure.getException().getClass().getName());
            testResult.put("failureTrace", failure.getTrace());
        }

        @Override
        public void testFinished(Description description) throws Exception {
            JSONObject testResult = jsonResult.getJSONObject(description.getMethodName());

            if(!testResult.has("successful")) {
                String testName = description.getMethodName();

                testResult.put("successful", true);
                jsonResult.put(testName, testResult);
            }
        }

        @Override
        public void testRunFinished(Result result) throws Exception {
            writeTestResult();
        }

        public synchronized void writeTestResult() {
            try(Writer outputWriter = Files.newBufferedWriter(outputPath)) {
                jsonResult.write(outputWriter);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(3);
            }
        }
    }
}
