JavaSubmissionTester::Engine.routes.draw do
  get 'pages/:page', to: 'pages#show', as: 'pages'
end
