require 'open3'
require 'java_submission_tester/java_helper'

module JavaSubmissionTester
  class JavaInputOutputJunitTestRunner
    include ::JavaSubmissionTester::JavaHelper
    LANGUAGE_NAME = 'Java8 Junit4'

    JUNIT_PATH = Pathname.new(File.dirname(__FILE__)).join('junit-4.12.jar').to_s
    HAMCREST_CORE_PATH = Pathname.new(File.dirname(__FILE__)).join('hamcrest-core-1.3.jar').to_s
    JUNIT_CLASSPATH = "#{JUNIT_PATH}:#{HAMCREST_CORE_PATH}"
    JUNIT_REPORT_PATH = Pathname.new(File.dirname(__FILE__)).join('JunitReport.jar').to_s

    def student_help_path
      ::JavaSubmissionTester::Engine.routes.url_helpers.pages_path(:student_help_io)
    end

    def teacher_help_path
      ::JavaSubmissionTester::Engine.routes.url_helpers.pages_path(:teacher_help_io_junit)
    end

    def validate_submission_structure(submission_file, test_file)

    end

    def validate_test_structure(test_file, stub_file)
      validation_message = validate_is_jar? test_file
      return validation_message unless validation_message.nil?

      run_unzipped test_file do |path|
        pathname = Pathname.new path

        validation_message = 'Test muss die Java Source Dateien enthalten!' if Dir[pathname + '**/*.java'].empty?
        validation_message = 'Test muss die Class Dateien enthalten!' if Dir[pathname + '**/*.class'].empty?

        break unless validation_message.nil?

        validation_message = validate_class_version Dir[pathname + '**/*.class'].first

        break unless validation_message.nil?

        compiled, error = compiles?(path)
        validation_message = ['Test kann nicht compiliert werden!', error] unless compiled
      end

      return validation_message
    end

    def validate_input_generator_structure(test_file, stub_file)
      validation_message = validate_is_jar? stub_file
      return validation_message unless validation_message.nil?

      run_unzipped stub_file do |path|
        pathname = Pathname.new path

        validation_message = 'Test muss die Java Source Dateien enthalten!' if Dir[pathname + '**/*.java'].empty?
        validation_message = 'Test muss die Class Dateien enthalten!' if Dir[pathname + '**/*.class'].empty?

        break unless validation_message.nil?

        validation_message = validate_class_version Dir[pathname + '**/*.class'].first

        break unless validation_message.nil?

        runnable, error, out = runnable?(stub_file.current_path)

        validation_message = 'Input Generator produziert keinen Input auf stdout!', error if out.strip.empty?
        validation_message = 'Input Generator ist nicht ausführbar!', error unless runnable
      end

      return validation_message
    end

    def test(input_output_task, submission, test_suit)
      user = submission.user
      input = input_output_task.input(user)

      run_test(input_output_task.test.current_path, input.input.current_path, submission.output.current_path, test_suit)
    end

    private

    def run_test(jar_file, input_file_path, submission_path, test_suit)
      output_file = Tempfile.new
      output_file.close

      cmd = "#{JavaSubmissionTester.java_8_path[:java]} -Dinput=\"#{input_file_path}\" -Dstudent-output=\"#{submission_path}\" -cp \"#{JUNIT_REPORT_PATH}:#{JUNIT_CLASSPATH}:#{jar_file}\" de.thm.mni.JunitJSONReportRunner \"#{output_file.path}\" \"#{test_suit}\""
      stdout_string, stderr_string, status = Open3.capture3(cmd, stdin_data: File.read(input_file_path))

      output_file.open
      result = JSON.load output_file
      output_file.close! #close and Delete tempfile

      return result
    end
  end
end