require 'open3'
require 'java_submission_tester/java_helper'

module JavaSubmissionTester
  class JavaSubmissionTestRunner
    include ::JavaSubmissionTester::JavaHelper

    LANGUAGE_NAME = 'Java'
    MOSS_LANGUAGE = 'java'
    TEST_TIMEOUT_DURATION = '2m'

    def student_help_path
      ::JavaSubmissionTester::Engine.routes.url_helpers.pages_path(:student_help_submission)
    end

    def teacher_help_path
      ::JavaSubmissionTester::Engine.routes.url_helpers.pages_path(:teacher_help_submission)
    end

    def validate_test_structure(test_file, stub_file)
      validation_message = validate_is_jar? test_file
      validation_message = validate_is_jar? stub_file

      return validation_message unless validation_message.nil?

      run_unzipped test_file do |path|
        pathname = Pathname.new path

        validation_message = 'Test muss die Java Source Dateien enthalten!' if Dir[pathname + '**/*.java'].empty?
        validation_message = 'Test muss die Class Dateien enthalten!' if Dir[pathname + '**/*.class'].empty?

        break unless validation_message.nil?

        compiled, error = compiles?(path)
        validation_message = 'Test compiliert ohne Abgabe. Test enthält vermutlich die zu testende Klasse und verdeckt sie somit. Bitte entfernen sie die zu testende Klasse aus der Jar-Datei.' if compiled

        break unless validation_message.nil?

        unzip stub_file.current_path, path

        compiled, error = compiles?(path)
        validation_message = ['Test kann mit dem angegeben Stub nicht compiliert werden.', error] unless compiled
      end

      return validation_message
    end

    def validate_submission_structure(submission_file, test_file)
      validation_message = validate_is_jar? submission_file

      return validation_message unless validation_message.nil?

      run_unzipped submission_file do |path|
        pathname = Pathname.new path

        validation_message = 'Abgabe muss die Java Source Dateien enthalten!' if Dir[pathname + '**/*.java'].empty?
        validation_message = 'Abgabe muss die Class Dateien enthalten!' if Dir[pathname + '**/*.class'].empty?

        break unless validation_message.nil?

        unzip test_file.current_path, path # Unzip now because it will break the above checks

        compiled, error = compiles?(path)
        validation_message = ['Abgabe kann nicht compiliert werden.', error] unless compiled
      end

      return validation_message
    end

    def moss_language
      MOSS_LANGUAGE
    end

    # calls block with all source file contents that may be relevant to moss
    def source_files(carrierwave_uploader)
      run_unzipped carrierwave_uploader do |path|
        path_name = Pathname.new path

        source_files = Dir[path_name + '**/*.java']

        source_files.each do |source_file|
          yield File.basename(source_file), File.open(source_file)
        end
      end
    end

    def test(submission_test, submission, test_suit)
      result = nil

      run_unzipped submission.submission, submission_test.test do |working_dir|
        policy_file = Tempfile.new 'deny_all.policy'
        write_policy policy_file, submission.submission.current_path, submission_test.test.current_path, working_dir
        policy_file.close

        compiles, error = compiles? working_dir

        output_file = Tempfile.new
        output_file.close
        cmd = %W[
        timeout #{TEST_TIMEOUT_DURATION}
        #{JavaSubmissionTester.java_8_path[:java]} -Djava.security.manager -Djava.security.policy=#{policy_file.path} -cp
        #{::JavaSubmissionTester::JavaHelper::JUNIT_REPORT_PATH}:#{::JavaSubmissionTester::JavaHelper::JUNIT_CLASSPATH}:#{submission_test.test.current_path}#{compiles ? ':.' : ''}:#{submission.submission.current_path}
        de.thm.mni.JunitJSONReportRunner #{output_file.path} #{test_suit}
        ]

        stdout_string, stderr_string, status = Open3.capture3(*cmd, chdir: working_dir) # => outputs to output_file

        output_file.open
        result = JSON.load output_file
        output_file.close! #close and Delete tempfile
      end

      return result
    end

    private
    def write_policy(file, submission_codebase_path, test_path, working_dir)
      #TODO I may need to change JUNIT_REPORT_PATH to a path that uses / instead of \ on windows
      policy_file_content = "grant codeBase \"file:#{JavaHelper::JUNIT_PATH}\" {permission java.security.AllPermission \"\", \"\";};" +
          "grant codeBase \"file:#{JavaHelper::HAMCREST_CORE_PATH}\" {permission java.security.AllPermission \"\", \"\";};" +
          "grant codeBase \"file:#{JavaHelper::JUNIT_REPORT_PATH}\" {permission java.security.AllPermission \"\", \"\";};" +
          "grant codeBase \"file:#{test_path}\" {permission java.security.AllPermission \"\", \"\";};" +
          "grant codeBase \"file:#{submission_codebase_path}\" {permission java.io.FilePermission \"#{working_dir}/*\", \"read,write\";};"
      file.write policy_file_content
    end
  end
end