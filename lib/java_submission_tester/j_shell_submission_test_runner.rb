require 'zip'
require 'open3'
require 'mimemagic'

module JavaSubmissionTester
  class JShellSubmissionTestRunner
    LANGUAGE_NAME = 'JShell'
    MOSS_LANGUAGE = 'java'
    JUNIT_CODEBASE_PATH = '/usr/share/java/*'
    JUNIT_CLASSPATH = '/usr/share/java/junit.jar:/usr/share/java/hamcrest/core.jar'
    JUNIT_REPORT_PATH = Pathname.new(File.dirname(__FILE__)).join('JunitReport.jar').to_s
    TEST_TIMEOUT_DURATION = '2m'

    # test if the content type for the test file is allowed return the error message to show if not allowed
    def validate_test_mime_type(test_uploader)
      mime_type_path = MimeMagic.by_path(test_uploader.current_path)
      mime_type_magic = MimeMagic.by_magic(test_uploader.file.read)

      return 'Dateityp muss jar sein' unless mime_type_path == 'application/x-java-archive' || mime_type_path == 'application/java-archive'
      return 'Dateityp muss jar sein' unless mime_type_magic == 'application/x-java-archive' || mime_type_magic == 'application/java-archive' || mime_type_magic == 'application/zip'
    end

    # test if the content type for the submission file is allowed return the error message to show if not allowed
    def validate_submission_mime_type(submission_uploader)
      mime_type_path = MimeMagic.by_path(submission_uploader.current_path)

      return 'Dateityp muss .java sein' unless mime_type_path == 'text/x-java' || mime_type_path == 'text/java'
    end

    def validate_submission_structure(submission_file, test_file)
    end

    def validate_test_structure(test_file, stub_file)
      return 'Test muss die Java Source Dateien enthalten!' unless any_java_files?(test_file)
      return 'Test muss die Class Dateien enthalten!' unless any_class_files?(test_file)

      compiled, error = compiles?(test_file, nil)

      return "Test kann nicht compiliert werden.", error unless compiled
    end

    def moss_language
      MOSS_LANGUAGE
    end

    def test_suit_names_for(submission_test)
      find_test_suits submission_test.test.current_path
    end

    def test_cases_for_testsuit(submission_test, test_suit_name)

      begin
        class_path = "#{JUNIT_REPORT_PATH}:#{JUNIT_CLASSPATH}:#{submission_test.test.current_path}"
        class_path = "#{class_path}:#{submission_test.stub.current_path}" unless submission_test.stub.current_path.nil?

        cmd = "#{JavaSubmissionTester.java_9_path[:java]} -cp \"#{class_path}\" de.thm.mni.JunitJSONTestList \"#{test_suit_name}\""
        stdout_string, stderr_string, status = Open3.capture3(cmd)

        if status.exitstatus == 0
          result = JSON.parse stdout_string
          return result
        else
          raise "Test konnte mit dem übergebenen Stub nicht geladen werden. Java meldet:\n#{stderr_string}"
        end
      rescue JSON::ParserError => e
        Rails.logger.error "#{e.to_s}\n#{"Beim bestimmen der Testfälle wurde ungültiges JSON erstellt, dies sollte nicht möglich sein!\nstdout: #{stdout_string}\nstderr: #{stderr_string}"}"
        raise "Beim bestimmen der Testfälle wurde ungültiges JSON erstellt, dies sollte nicht möglich sein!\nstdout: #{stdout_string}\nstderr: #{stderr_string}"
      end
    end

    def test(submission_test, submission, test_suit)
      @policy_file = Tempfile.new 'deny_all.policy'
      write_policy @policy_file, submission.submission.current_path, submission_test.test.current_path
      @policy_file.close

      run_test submission.submission.current_path, submission_test.test.current_path, test_suit.name, @policy_file.path
    end

    # calls block with all source file contents that may be relevant to moss
    def source_files(submission, &block)
      iterate_zip_source_file_entries_as_io submission.submission.current_path, &block
    end

    private
    def run_test(submission_path, test_path, test_suit, policy_file)
      output_file = Tempfile.new
      output_file.close
      cmd = "timeout #{TEST_TIMEOUT_DURATION}" +
          " #{JavaSubmissionTester.java_9_path[:java]} -Djava.security.manager -Djava.security.policy=#{policy_file} -cp" +
          " \"#{JUNIT_REPORT_PATH}:" +
          "#{JUNIT_CLASSPATH}:" +
          "#{test_path}\"" +
          " -DjshellInput=\"#{submission_path}\" de.thm.mni.JunitJSONReportRunner #{output_file.path} #{test_suit}"
      stdout_string, stderr_string, status = Open3.capture3(cmd) # => outputs to outputfile
      output_file.open
      result = JSON.load output_file
      output_file.close! #close and Delete tempfile

      return result
    end

    def write_policy(file, submission_codebase_path, test_path)
      #TODO I may need to change JUNIT_REPORT_PATH to a path that uses / instead of \ on windows
      policy_file_content = "grant codeBase \"file:#{JUNIT_CODEBASE_PATH}\" {permission java.security.AllPermission \"\", \"\";};\n" +
          "grant codeBase \"file:#{JUNIT_REPORT_PATH}\" {permission java.security.AllPermission \"\", \"\";};\n" +
          "grant codeBase \"file:#{test_path}\" {permission java.io.FilePermission \"#{submission_codebase_path}\", \"read\";\n" +
          "permission java.util.PropertyPermission \"jshellInput\", \"read\";};\n" +
          "grant {permission com.sun.jdi.JDIPermission \"virtualMachineManager\", \"\";" +
          "permission java.lang.RuntimePermission \"modifyThreadGroup\", \"\";" +
          "permission java.lang.RuntimePermission \"modifyThread\", \"\";" +
          "permission java.lang.RuntimePermission \"accessClassInPackage.jdk.internal.jshell.debug\", \"\";" +
          "permission java.lang.RuntimePermission \"accessDeclaredMembers\", \"\";" +
          "permission java.util.PropertyPermission \"java.home\", \"read\";" +
          "permission java.util.PropertyPermission \"env.class.path\", \"read\";" +
          "permission java.util.PropertyPermission \"application.home\", \"read\";" +
          "permission java.net.SocketPermission \"127.0.0.1\", \"accept,resolve\";" +
          "permission java.util.PropertyPermission \"java.class.path\", \"read\";" +
          "permission java.io.FilePermission \"/usr/lib/jvm/-\", \"read\";" +
          "permission java.io.FilePermission \"/usr/share/java/-\", \"read\";" +
          "permission java.io.FilePermission \"#{JUNIT_REPORT_PATH}\", \"read\";" +
          "permission java.io.FilePermission \"#{test_path}\", \"read\";" +
          "permission java.io.FilePermission \"/usr/lib/jvm/-\", \"execute\";};\n" # I somehow have to give jshell some permissions but I don't know which jar
      file.write policy_file_content
    end

    def iterate_zip_entries_as_io(file, &block)
      Zip::InputStream.open(file) do |io|
        while (entry = io.get_next_entry)
          block.call entry.name, io
        end
      end
    end

    def iterate_zip_source_file_entries_as_io(file, &block)
      Zip::File.open(file) do |zip_file|
        test_suit_entries = zip_file.glob('**/*.java')

        test_suit_entries.each do |entry|
          entry.get_input_stream do |io|
            block.call File.basename(entry.name), io
          end
        end
      end
    end

    def find_test_suits(file)
      Zip::File.open(file) do |zip_file|
        test_suit_entries = zip_file.glob('**/Test*.class') | zip_file.glob('**/*Test.class')
        test_suit_entries.keep_if do |entry|
          entry.ftype != :directory
        end
        test_suit_entries.map do |entry|
          entry.name.sub(/.class$/, '').gsub(File::SEPARATOR, '.')
        end
      end
    end

    def any_class_files?(file)
      Zip::File.open(file) do |zip_file|
        zip_file.glob('**/*.class').any?
      end
    end

    def any_java_files?(file)
      Zip::File.open(file) do |zip_file|
        zip_file.glob('**/*.java').any?
      end
    end

    def compiles?(source, library)
      stdout_string, stderr_string, status = nil

      Dir.mktmpdir do |dir|
        unzip source, Pathname.new(dir)
        unzip library, Pathname.new(dir) unless library.nil?

        class_path = "#{JUNIT_REPORT_PATH}:#{JUNIT_CLASSPATH}#{library.nil? ? '' : ":#{library}"}:#{source}"

        cmd = "#{JavaSubmissionTester.java_9_path[:javac]} -cp \"#{class_path}\" \"#{Dir["#{dir}/**/*.java"].join('" "')}\""

        stdout_string, stderr_string, status = Open3.capture3(cmd)
      end

      return status.exitstatus == 0, stderr_string
    end

    def unzip(file, dest_dir)
      Zip::File.open(file) do |zip_file|
        zip_file.each do |entry|
          output_path = dest_dir.join entry.name.strip
          FileUtils.mkdir_p output_path.dirname
          zip_file.extract entry, output_path unless File.exist? output_path
        end
      end
    end
  end
end