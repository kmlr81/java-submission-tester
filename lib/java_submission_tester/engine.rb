module JavaSubmissionTester
  class Engine < ::Rails::Engine
    isolate_namespace JavaSubmissionTester



    initializer "java_submission_tester.assets.precompile" do |app|
      app.config.assets.precompile += %w( java_submission_tester/*.png java_submission_tester/*.svg java_submission_tester/*.css java_submission_tester/*.js)
    end
  end
end
