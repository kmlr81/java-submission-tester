require 'zip'
require 'open3'
require 'mimemagic'

module JavaSubmissionTester
  module JavaHelper
    JUNIT_PATH = Pathname.new(File.dirname(__FILE__)).join('junit-4.12.jar').to_s
    HAMCREST_CORE_PATH = Pathname.new(File.dirname(__FILE__)).join('hamcrest-core-1.3.jar').to_s
    JUNIT_REPORT_PATH = Pathname.new(File.dirname(__FILE__)).join('JunitReport.jar').to_s
    JUNIT_CLASSPATH = "#{JUNIT_PATH}:#{HAMCREST_CORE_PATH}"

    def validate_is_jar?(carrierwave_uploader)
      mime_type_path = MimeMagic.by_path(carrierwave_uploader.current_path)
      mime_type_magic = MimeMagic.by_magic(carrierwave_uploader.file.read)

      return 'Dateityp muss jar sein' unless mime_type_path.nil? || mime_type_path == 'application/x-java-archive' || mime_type_path == 'application/java-archive'
      return 'Dateityp muss jar sein' unless mime_type_magic.nil? || mime_type_magic == 'application/x-java-archive' || mime_type_magic == 'application/java-archive' || mime_type_magic == 'application/zip'

      #Teste das eigentlich wichtige, kann ich es als Zip öffnen
      begin
        Zip::File.open(carrierwave_uploader.current_path) {}
      rescue
        return 'Dateityp muss jar sein'
      end
    end

    def test_suit_names_for(test, stub)
      test_suites = nil

      run_unzipped test do |path|
        path_name = Pathname.new path

        test_suite_selection = Dir[path_name + '**/Test*.class'] + Dir[path_name + '**/*Test.class']
        test_suite_selection.select! {|p| File.file? p}
        test_suite_selection.map! {|p| Pathname.new(p).relative_path_from(path_name).to_s}
        test_suite_selection.map! {|p| p.sub(/.class$/, '').gsub(::File::SEPARATOR, '.')}

        test_suites = test_suite_selection
      end

      return test_suites
    end

    def test_cases_for_testsuit(test, stub, test_suit_name)

      begin
        class_path = "#{::JavaSubmissionTester::JavaHelper::JUNIT_REPORT_PATH}:#{::JavaSubmissionTester::JavaHelper::JUNIT_CLASSPATH}:#{test.current_path}"
        class_path = "#{class_path}:#{stub.current_path}" unless stub.current_path.nil?

        cmd = "#{JavaSubmissionTester.java_8_path[:java]} -cp \"#{class_path}\" de.thm.mni.JunitJSONTestList \"#{test_suit_name}\""
        stdout_string, stderr_string, status = Open3.capture3(cmd)

        if status.exitstatus == 0
          result = JSON.parse stdout_string
          return result
        else
          raise "Test konnte mit dem übergebenen Stub nicht geladen werden. Java meldet:\n#{stderr_string}"
        end
      rescue JSON::ParserError => e
        Rails.logger.error "#{e.to_s}\n#{"Beim bestimmen der Testfälle wurde ungültiges JSON erstellt, dies sollte nicht möglich sein!\nstdout: #{stdout_string}\nstderr: #{stderr_string}"}"
        raise "Beim bestimmen der Testfälle wurde ungültiges JSON erstellt, dies sollte nicht möglich sein!\nstdout: #{stdout_string}\nstderr: #{stderr_string}"
      end
    end

    def run_unzipped(*carrierwave_uploaders)
      if block_given?
        Dir.mktmpdir do |tmp_dir|
          carrierwave_uploaders.each do |carrierwave_uploader|
            unzip(carrierwave_uploader.current_path, tmp_dir)
          end

          yield tmp_dir
        end
      else
        raise 'run_unzipped needs a block to run!'
      end
    end

    def generate_input(input_generator_uploader)
      status_ok, std_err, std_out = runnable?(input_generator_uploader.current_path)

      raise "There was an problem with the input Generator\nExcecution ended with error:\n#{std_err}" unless status_ok
      return std_out
    end

    def runnable?(jar_file)
      cmd = "#{JavaSubmissionTester.java_8_path[:java]} -jar \"#{jar_file}\""
      stdout_string, stderr_string, status = Open3.capture3(cmd)

      return status.exitstatus == 0, stderr_string, stdout_string
    end

    def validate_class_version(class_file_path)
      cmd = "#{JavaSubmissionTester.java_8_path[:javap]} -verbose #{class_file_path} | grep major"

      stdout_string, stderr_string, status = Open3.capture3(cmd)

      version = /\d+/.match stdout_string.strip
      return 'Abgabe wurde compiliert mit Java >= 9. Unterstützt werden Java <= 8' if version[0].to_i > 52
    end

    def compiles?(working_dir)
      class_path = "#{JUNIT_REPORT_PATH}:#{JUNIT_CLASSPATH}:#{working_dir}"

      cmd = "#{JavaSubmissionTester.java_8_path[:javac]} -cp \"#{class_path}\" \"#{Dir["#{working_dir}/**/*.java"].join('" "')}\""

      stdout_string, stderr_string, status = Open3.capture3(cmd)

      return status.exitstatus == 0, stderr_string
    end

    def unzip(file, dest_dir)
      dest_dir = Pathname.new dest_dir
      Zip::File.open(file) do |zip_file|
        zip_file.each do |entry|
          output_path = dest_dir.join entry.name.strip
          FileUtils.mkdir_p output_path.dirname
          zip_file.extract(entry, output_path) {true}
        end
      end
    end
  end
end