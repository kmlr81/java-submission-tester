
require 'open3'
require 'java_submission_tester/java_helper'

module JavaSubmissionTester
  class ::JavaSubmissionTester::JavaInputOutputTestRunner
    include JavaHelper

    LANGUAGE_NAME = 'Java8'

    def student_help_path
      ::JavaSubmissionTester::Engine.routes.url_helpers.pages_path(:student_help_io)
    end

    def teacher_help_path
      ::JavaSubmissionTester::Engine.routes.url_helpers.pages_path(:teacher_help_io)
    end

    def validate_submission_structure(submission_file, test_file)

    end

    def validate_test_structure(test_file, stub_file)
      validation_message = validate_is_jar? test_file

      return validation_message unless validation_message.nil?

      runnable, error = runnable?(test_file)
      return 'Test ist nicht ausführbar!', error unless runnable
    end

    def validate_input_generator_structure(test_file, stub_file)
      validation_message = validate_is_jar? stub_file

      return validation_message unless validation_message.nil?

      runnable, error, out = runnable?(stub_file)
      return 'Input Generator ist nicht ausführbar!', error unless runnable
      return 'Input Generator produziert keinen Input auf stdout!', error if out.strip.empty?
    end

    def test_suit_names_for(test, stub)
      ['InputOutput']
    end

    def test_cases_for_testsuit(test, stub, test_suit_name)
      {
          'Test Output' => {
              'points' => 0
          }
      }
    end

    def test(input_output_task, submission, test_suit)
      user = submission.user
      input = input_output_task.input(user)

      message = run_test(input_output_task.test.current_path, input.input.current_path, submission.output.current_path)

      success = (message.nil? || message.strip.empty?)
      failure_message = nil
      failure_trace = nil

      unless message.nil?
        failure_message = message.lines.first
        failure_trace = message
      end

      {
          'Test Output' => {
              'successful' => success,
              'failureMessage' => failure_message,
              'failureTrace' => failure_trace
          }
      }
    end

    private
    def run_test(jar_file, input_file_path, submission_path)
      cmd = "#{JavaSubmissionTester.java_8_path[:java]} -jar \"#{jar_file}\" input=\"#{input_file_path}\" student-output=\"#{submission_path}\""
      stdout_string, stderr_string, status = Open3.capture3(cmd, stdin_data: File.read(input_file_path))

      raise "There was an problem with the example solution while testing the input:\n#{File.read(input_file_path)}\n\nExcecution ended with error:\n#{stderr_string}" unless status.exitstatus == 0
      return stdout_string
    end
  end
end