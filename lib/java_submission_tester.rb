require 'java_submission_tester/engine'

module JavaSubmissionTester
  # Your code goes here...
  mattr_accessor :java_8_path
  mattr_accessor :java_9_path

  self.java_8_path = {
      java: '/usr/lib/jvm/java-8-openjdk/bin/java',
      javac: '/usr/lib/jvm/java-8-openjdk/bin/javac',
      javap: '/usr/lib/jvm/java-8-openjdk/bin/javap'
  }
  self.java_9_path = {
      java: '/usr/lib/jvm/java-9-jdk/bin/java',
      javac: '/usr/lib/jvm/java-9-jdk/bin/javac',
      javap: '/usr/lib/jvm/java-9-jdk/bin/javap'
  }
end
