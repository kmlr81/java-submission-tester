/**
 * Created by dominic-althaus on 26.05.17.
 */
$(document).ready(function() {
  $('img.zoomable').click(function() {
    $(this).toggleClass('large');
  });
})