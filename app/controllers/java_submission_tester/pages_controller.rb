module JavaSubmissionTester
  class PagesController < ::ApplicationController
    layout "application"

    def show
      puts params[:page]
      render action: params[:page]
    end
  end
end